<?php
namespace AppBundle\Security;
use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;
use Symfony\Component\Security\Guard\GuardAuthenticatorInterface;
use Symfony\Component\Serializer\Encoder\EncoderInterface;

/**
 * Created by PhpStorm.
 * User: Richard
 * Date: 06.12.2015
 * Time: 14.40
 */
class FormLoginAuthenticator extends AbstractGuardAuthenticator
{
    private $router;
    private $encoder;
    private $logger;
    private $encoderFactory;

    public function __construct(Router $router,UserPasswordEncoder $encoder, LoggerInterface $logger)
    {
        $this->router = $router;
        $this->encoder = $encoder;
        $this->logger = $logger;
    }

    public function start(Request $request, AuthenticationException $authException = null)
    {
        $url = $this->router->generate("login");
        $this->logger->debug("Start url: $url",[]);
        return new RedirectResponse($url);
    }

    public function getCredentials(Request $request)
    {
        if ($request->getPathInfo() != '/login_check')
            return null;

        return [
            'email' => $request->get("email"),
            'password' => $request->request->get("password")
        ];
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $user = $userProvider->loadUserByUsername($credentials['email']);

        $this->logger->debug("getUser",['user' => $user]);
        return $user;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {
        $plainPassword = $credentials['password'];

        return $this->encoder->isPasswordValid($user, $plainPassword);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $url = $this->router->generate("login");
        return new RedirectResponse($url);
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $url = $this->router->generate("inbox");
        return new RedirectResponse($url);
    }

    public function supportsRememberMe(){ return true; }
}