<?php

namespace AppBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints\Email;

/**
 * User
 */
class User implements UserInterface, \JsonSerializable
{
    //<editor-fold desc="Properties">
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * @var int
     */
    private $mobile;

    /**
     * @var string
     */
    private $authId;

    /**
     * @var string
     */
    private $authToken;

    /**
     * @var Message[]
     */
    private $messages;


    //</editor-fold>

    //<editor-fold desc="Getters">
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @return string
     */
    public function getAuthId()
    {
        return $this->authId;
    }

    /**
     * Get authToken
     *
     * @return string
     */
    public function getAuthToken()
    {
        return $this->authToken;
    }

    /**
     * @return Message[]
     */
    public function getMessages()
    {
        $messages = clone $this->messages;
        return $messages;
    }
    //</editor-fold>

    //<editor-fold desc="Constructors">
    /**
     * User constructor.
     * @param string $email
     * @param string $password
     * @param string $authId
     * @param string $authToken
     */
    private function __construct($email, $password, $mobile, $authId, $authToken)
    {
        $this->email = $email;
        $this->password = $password;
        $this->mobile = $mobile;
        $this->authId = $authId;
        $this->authToken = $authToken;

        $this->messages = new ArrayCollection();
    }

    public static function register($email, $password,$mobile, $authId, $authToken)
    {
        if (!$email || !$password || !$authId || !$authToken || !$mobile)
            throw new BadRequestHttpException("Some required parameters are missing!");

        return new self($email,$password, $mobile,$authId, $authToken);
    }
    //</editor-fold>

    //<editor-fold desc="UserInterface">

    public function getRoles()
    {
        return ["ROLE_USER"];
    }

    public function getSalt()
    {
        return null;
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
    //</editor-fold>

    public function jsonSerialize()
    {
        return [
            'email' => $this->email,
            'auth_id' => $this->authId,
        ];
    }

    public function sendMessage($dst, $message)
    {
        $msg = Message::send($this, $dst, $message);
        $this->messages->add($msg);
        return $msg;
    }
}

