<?php

namespace AppBundle\Entity;

/**
 * Message
 */
class Message implements \JsonSerializable
{
    const STATUS_URL = "https://messenger.richardhagen.no/api/status_message";

    //<editor-fold desc="Properties">
    /**
     * @var int
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var int
     */
    private $src;

    /**
     * @var int
     */
    private $dst;

    /**
     * @var string
     */
    private $message;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $uuid;

    /**
     * @var \DateTime
     */
    private $createdAt;

    private $state;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getState()
    {
        return $this->state;
    }
    //</editor-fold>

    //<editor-fold desc="Getters">
    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return int
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @return int
     */
    public function getDst()
    {
        return $this->dst;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    //</editor-fold>

    //<editor-fold desc="Constructors">
    /**
     * Message constructor.
     * @param User $user
     * @param int $src
     * @param int $dst
     * @param string $message
     * @param string $type
     * @param string $uuid
     */
    private function __construct(User $user, $src, $dst, $message, $type, $uuid, $state)
    {
        $this->user = $user;
        $this->src = $src;
        $this->dst = $dst;
        $this->message = $message;
        $this->type = $type;
        $this->uuid = $uuid;
        $this->state = $state;

        $this->createdAt = new \DateTime();
    }

    public static function send(User $user, $dst, $message)
    {
        return new self($user, $user->getMobile(), $dst,$message, "sms","", 1);
    }

    public static function receive(User $user, $src, $message, $uuid)
    {
        return new self($user, $src, $user->getMobile(),$message,"sms",$uuid,2);
    }
    //</editor-fold>

    //<editor-fold desc="Behavior">
    public function getPlivoData()
    {
        return [
            'src' => $this->src,
            'dst' => $this->dst,
            'text' => $this->message,
            'url' => self::STATUS_URL,
            'log' => true,
        ];
    }

    public function setUUID($uuid)
    {
        $this->uuid = $uuid;
    }

    public function isDelivered()
    {
        $this->state = 2;
    }

    public function isError()
    {
        $this->state = 0;
    }

    public function jsonSerialize()
    {
        return [
            'src' => $this->src,
            'dst' => $this->dst,
            'message' => $this->message,
            'state' => $this->state,
            'uuid' => $this->uuid
        ];
    }
    //</editor-fold>
}

