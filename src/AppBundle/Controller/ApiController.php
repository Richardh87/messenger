<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class apiController
 * @package AppBundle\Controller
 * @Route("/api")
 */
class ApiController extends Controller
{

    /**
     * @param Request $request
     * @return Response
     * @Route("/receive_message", name="receive_message")
     * @throws NotFoundHttpException
     */
    public function receiveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $destinationAddr = $request->get("To");
        $mobile = $request->get("From");
        $message = $request->get("Text");
        $referenceId = $request->get("MessageUUID");

        $user = $this->getDoctrine()->getRepository("AppBundle:User")->findOneBy(["mobile" => $destinationAddr]);
        if (!$user)
            throw new NotFoundHttpException("Destination number ($destinationAddr) not found");

        $msg = Message::receive($user,$mobile,$message,$referenceId);
        $em->persist($msg);
        $em->flush();

        return new Response();
    }

    /**
     * @param Request $request
     * @return Response
     * @Route("/status_message", name="status_message")
     */
    public function statusAction(Request $request)
    {
        $status = $request->get("Status");
        $uuid = $request->get("ParentMessageUUID");

        $em = $this->getDoctrine()->getManager();
        /** @var Message $msg */
        $msg = $em->getRepository("AppBundle:Message")->findOneBy(["uuid" => $uuid]);
        if (!$msg) throw new NotFoundHttpException("Message with UUID '$uuid'' not found!");


        if ($status == 'delivered' || $status == 'sent')
            $msg->isDelivered();
        else {
            $msg->isError();
            $this->get("logger")->warning("Message not sent!", $msg->jsonSerialize());
        }

        return new Response();
    }
}
