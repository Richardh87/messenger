<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Entity\User;
use Monolog\Logger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use \Plivo\RestAPI as Plivo;

/**
 * Class inboxController
 * @package AppBundle\Controller
 * @Route("/inbox")
 * @Security("has_role('ROLE_USER')")
 */
class inboxController extends Controller
{
    /**
     * @Route("/",name="inbox")
     * @Template(":inbox:inbox.html.twig")
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @Route("/send_message", name="send_message")
     */
    public function sendAction(Request $request)
    {

        $dst = $request->get('dst');
        $message = $request->get('message');
        $msg = $this->getUser()->sendMessage($dst,$message);

        $this->sendMessage($msg);
        $this->addFlash("Message sent to $dst", "notice");

        $this->getDoctrine()->getManager()->flush();
        return $this->redirectToRoute("inbox");

    }

    /**
     * @param $msg
     * @return mixed
     */
    protected function sendMessage(Message $msg)
    {
        /** @var User $user */
        $user = $msg->getUser();
        /** @var Logger $logger */
        $logger = $this->get("logger");


        $plivo = new Plivo($user->getAuthId(), $user->getAuthToken());
        $response = $plivo->send_message($msg->getPlivoData());
        $logger->debug("message sent to plivo", $response);
        $uuid = $response['response']['message_uuid'][0];
        $msg->setUUID($uuid);
    }
}