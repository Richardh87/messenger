<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    /**
     * @Route("/", name="login")
     * @Template(":auth:login.html.twig")
     */
    public function indexAction()
    {
        return [];
    }

    /**
     * @param Request $request
     * @return array
     * @Route("/register", name="register")
     * @Template(":auth:register.html.twig")
     */
    public function registerAction(Request $request)
    {
        if ($request->isMethod("post"))
        {
            $password = $request->get("password");
            $encoderFactory = $this->get("security.encoder_factory");
            $encoder = $encoderFactory->getEncoder("AppBundle\\Entity\\User");
            $password = $encoder->encodePassword($password,"");

            $email = $request->get("email");
            $user = User::register(
                $email,
                $password,
                $request->get("mobile"),
                $request->get("auth_id"),
                $request->get("auth_token")
                );

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->addFlash("Bruker $email opprettet", "notice");
            return $this->redirectToRoute("login");
        }

        return [];
    }
}
